import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import HeroSection from './Component/HeroSection/HeroSection';

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
      <HeroSection/>
    </div>
  )
}

export default App
