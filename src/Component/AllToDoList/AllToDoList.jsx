import Button from 'react-bootstrap/Button';
import React, { useContext } from 'react'
import { items } from '../HeroSection/HeroSection'

export default function AllToDoList() {
    const [state,dispatch] = useContext(items)
    let allToDoList
    if(Array.isArray(state) && state.length>0){
        allToDoList = state.map((item) => {
            return (
                <div key={item.id} className='m-2' >
                    <input onChange={()=>dispatch({type:"completed", id:item.id})} checked={item.completed} type="checkbox" name="" id="" />
                    <input className='m-1' type="text" name="" id="" />
                    <Button onClick={()=>dispatch({type:"delete", id:item.id})} variant="danger">Delete</Button>{' '}
                </div>
            )
        })
    }
    return (
        <div>
            {allToDoList}
        </div>
    )
}
