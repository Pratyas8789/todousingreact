import React, { createContext, useEffect, useReducer } from 'react'
import Button from 'react-bootstrap/Button';
import AllToDoList from '../AllToDoList/AllToDoList';
import { useRef } from 'react';

const reducer = (state, action) => {
    switch (action.type) {
        case "add": {
            if(state===null){
                state=[]
            }
            return [
                ...state,
                {
                    id: Date.now(),
                    text: "",
                    completed: false
                }
            ]
        }
        case "delete": {
            return state.filter(item => item.id !== action.id)
        }
        case "completed": {
            return state.map((item) => {
                if (item.id === action.id) {
                    return {
                        ...item,
                        completed: !item.completed,
                    }
                }
                return item
            })
        }
        case "reset": {
            return action.payload;
        }
        default: {
            return state
        }
    }
}

export const items = createContext()
export default function HeroSection() {
    const [state, dispatch] = useReducer(reducer, [])  

    
    const didRun = useRef(false)

    useEffect(() => {
        if (!didRun.current) {
            const raw = localStorage.getItem('data')
            dispatch({ type: "reset", payload: JSON.parse(raw) })
            didRun.current = true
        }
    })

    useEffect(() => {
        if(state==null){
            const emptyArr=[]
            localStorage.setItem('data', JSON.stringify(emptyArr))
        }else{
            localStorage.setItem('data', JSON.stringify(state))
        }
    }, [state])

    return (
        <div className='d-flex flex-column align-items-center mt-4' >
            <items.Provider value={[state, dispatch]} >
                <h1>To-Do</h1>
                <Button onClick={() => dispatch({ type: "add" })} variant="light">New Todo</Button>{' '}
                <br />
                <br />
                <AllToDoList />
            </items.Provider>
        </div>
    )
}
